## Your spot on earth !

idea:
 every physical event can be spotted on a time-space *hyper*sphere
 

proof of existance: recent point on trajectory "public-name,time,place"

- distance on "2D" sphere
- distance on 2D map
- distance on 1D Hilbert-curve
- distance on 1D Z-curve
 


## clone our project: 

 - git clone git@gitlab.com:xn--tg8h/spotted.git

